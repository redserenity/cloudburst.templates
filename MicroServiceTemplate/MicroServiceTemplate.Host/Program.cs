﻿using MicroServiceTemplate.Core;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace MicroServiceTemplate.Host {
	public class Program {
		public static void Main(string[] args) => 
			WebHost.CreateDefaultBuilder(args)
				.UseStartup<Startup>()
				.BuildMicroServiceHost()
				.Run();
	}
}