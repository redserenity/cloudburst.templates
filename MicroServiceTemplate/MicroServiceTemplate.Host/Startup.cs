﻿using MicroServiceTemplate.Core;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace MicroServiceTemplate.Host {
	public class Startup {
		private readonly IConfiguration _configuration;

		public Startup(IConfiguration configuration) {
			_configuration = configuration;
		}

		public void ConfigureServices(IServiceCollection services) {
			services.AddMicroService();
			// Configure HealthChecks
		}

		public void Configure(IApplicationBuilder app, IHostingEnvironment env) {
			if (env.IsDevelopment()) {
				app.UseDeveloperExceptionPage();
			}

			app.UseMicroService();
			app.UseEndpoints(configure => {
				configure.Add
			});

			app.Run(async (context) => { await context.Response.WriteAsync("Hello World!"); });
		}
	}
}
