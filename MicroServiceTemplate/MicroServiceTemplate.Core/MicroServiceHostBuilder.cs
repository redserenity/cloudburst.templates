using Microsoft.AspNetCore.Hosting;

namespace MicroServiceTemplate.Core {
	public static class MicroServiceHostBuilder {
		public static IWebHost BuildMicroServiceHost(this IWebHostBuilder webHostBuilder) {

			// Serilog
			// Health Checks?

			return webHostBuilder.Build();
		}
	}
}