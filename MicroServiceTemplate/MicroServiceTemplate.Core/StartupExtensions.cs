﻿using MicroServiceTemplate.Core.Health;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;

namespace MicroServiceTemplate.Core {
	public static class StartupExtensions {
		public static IServiceCollection AddMicroService(this IServiceCollection services) {
			// Configure Services here

			services.AddMicroServiceHealthChecks();

			return services;
		}

		public static IApplicationBuilder UseMicroService(this IApplicationBuilder app) {
			// Configure Application here

			app.UseMicroServiceHealthChecks();

			return app;
		}

	}
}