using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace MicroServiceTemplate.Core.Health {
	public static class HealthStartupExtensions {
		public static IServiceCollection AddMicroServiceHealthChecks(this IServiceCollection services) {
			IHealthChecksBuilder healthChecksBuilder = services.AddHealthChecks();
			return services;
		}

		public static IApplicationBuilder UseMicroServiceHealthChecks(this IApplicationBuilder app) {
			// TODO: Make this configurable in appsettings.json
			app.UseHealthChecks("/health", new HealthCheckOptions {
				AllowCachingResponses = false,
				ResultStatusCodes = {
					[HealthStatus.Healthy] = StatusCodes.Status200OK,
					[HealthStatus.Degraded] = StatusCodes.Status424FailedDependency,
					[HealthStatus.Unhealthy] = StatusCodes.Status503ServiceUnavailable
				},
				ResponseWriter = HealthResponseWriter.WriteResponse
			});
			return app;
		}
	}
}
