﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace MicroServiceTemplate.Api.Controllers {

	[Route("api/example")]
	[ApiController]
	public class ExampleController : Controller {

		[Route("")]
		[HttpGet]
		public async Task<IActionResult> Example() {
			return await Task.FromResult(Ok("Hello, World"));
		}
	}
}